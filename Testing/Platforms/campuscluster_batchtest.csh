#!/bin/tcsh -fe

set app=${1}
set nodes=${2}
set appargs="${3}"
set joboutfile="quicktest.out"
rm -f ${joboutfile} quicktest.csh
@ nprocs = ( 12 * ${nodes} )
cat <<EOF > ./quicktest.csh
#!/bin/tcsh
#PBS -j oe
#PBS -o ${joboutfile}
#PBS -l nodes=${nodes}:ppn=12
#PBS -l walltime=00:30:00 
#PBS -q cse
#PBS -W group_list=csar

cd \${PBS_O_WORKDIR}

printf "Hostfile: \${PBS_NODEFILE}\n"
printf "-------------------------\n"
cat \${PBS_NODEFILE}
#env
printf "-------------------------\n"
printf "mpirun -n ${nprocs} -machinefile \${PBS_NODEFILE} ${app} ${appargs}\n"
mpirun -n ${nprocs} -machinefile \${PBS_NODEFILE} ${app} ${appargs}
EOF
set echo
set jobid=`qsub -q cse quicktest.csh`
unset echo
@ i = 1
while($i <= 720)
    @ i += 1
    if( -e ${joboutfile} ) then
        @ i += 721;
    else
        sleep 5;
    endif
end
printf "Quicktest complete.\n"
#rm -f ./codelet4_quicktest.csh
