#!/bin/tcsh

set RESULTSFILE = ${1}
set SRCDIR = ${2}
set BINDIR = ${3}

rm -f tmpresults_1.txt
srun -n 16 -ppdebug -t 5 ${BINDIR}/codelet4_parallel_test -o tmpresults_1.txt
if(! -e tmpresults_1.txt) then
  exit 1
else
 mv tmpresults_1.txt ${RESULTSFILE}
endif
exit 0
