
SUBROUTINE INITSoln(NV,U)
  INTEGER(KIND=8), INTENT(IN) :: NV
  REAL(KIND=8), INTENT(INOUT) :: U(NV)

!$OMP PARALLEL 
!$OMP DO 
  DO I = 1,NV
     U(I) = 0.0
  END DO
!$OMP END DO
!$OMP END PARALLEL

END SUBROUTINE INITSoln

SUBROUTINE RK4(NV, U, K1, K2, K3, K4)

  INTEGER(KIND=8), INTENT(IN) :: NV
  REAL(KIND=8), INTENT(IN)    :: K1(NV), K2(NV), K3(NV), K4(NV)
  REAL(KIND=8), INTENT(INOUT) :: U(NV)

  INTEGER(KIND=8) :: I
  REAL(KIND=8),PARAMETER :: fac16 = 1.0_8 / 6.0_8

  DO I = 1, NV
    U(I) = U(I) + fac16 * (K1(I) + 2.0_8 * (K2(I) + K3(I)) + K4(I))
  END DO

END SUBROUTINE RK4

SUBROUTINE RK4T(NV, H, U, K1, K2, K3, K4)

  INTEGER(KIND=8), INTENT(IN) :: NV
  REAL(KIND=8), INTENT(IN)    :: H
  REAL(KIND=8), INTENT(IN)    :: K1(NV), K2(NV), K3(NV), K4(NV)
  REAL(KIND=8), INTENT(INOUT) :: U(NV)

  INTEGER(KIND=8) :: I
  REAL(KIND=8),PARAMETER :: fac1o6  = 1.0_8 / 6.0_8
  REAL(KIND=8),PARAMETER :: fac1o24 = 1.0_8 / 24.0_8
  REAL(KIND=8) :: H2, H3, H4
  
  H2 = H*H
  H3 = H2*H
  H4 = H3*H

!$OMP PARALLEL
!$OMP DO
  DO I = 1, NV
    U(I) = U(I)+ H*K1(I) + 0.5_8*H2*K2(I) + fac1o6*H3*K3(I) + fac1o24*H4*K4(I)
  END DO
!$OMP END DO
!$OMP END PARALLEL

END SUBROUTINE RK4T


SUBROUTINE RHSINTERIOR(N1, N2, N3, DX, U, KN)

  INTEGER(KIND=8), INTENT(IN) :: N1, N2, N3
  REAL(KIND=8), INTENT(IN)    :: U(N1*N2*N3), DX
  REAL(KIND=8), INTENT(OUT)   :: KN(N1*N2*N3)

  INTEGER(KIND=8) :: I, J, K, LI, LIK, LIJ
  REAL(KIND=8)    :: fac
  INTEGER(KIND=8) :: plane

  fac = -0.5_8 / DX
  plane = (N1*N2)

!$OMP PARALLEL
!$OMP DO PRIVATE(K,J,LIK,LIJ,LI)
  DO K = 1, N3
     LIK = (K-1)*plane
    DO J = 1, N2
       LIJ = (J-1)*N1 + LIK
      DO I = 2, N1-1
        LI = LIJ + I
        KN(LI) = ( U(LI+1) - U(LI-1) ) * FAC
      END DO
    END DO
  END DO
!$OMP END DO
!$OMP END PARALLEL

END SUBROUTINE RHSINTERIOR

! Use non-zero tau to specify boundary data
SUBROUTINE RHSLEFTBOUNDARY(N1, N2, N3, DX, U, Tau, u0, KN)

  INTEGER(KIND=8), INTENT(IN) :: N1, N2, N3
  REAL(KIND=8), INTENT(IN)    :: U(N1*N2*N3), DX, Tau, u0
  REAL(KIND=8), INTENT(OUT)   :: KN(N1*N2*N3)
  INTEGER(KIND=8) :: J, K, LI, plane
  REAL(KIND=8)    :: fac,bcfac,bcterm

  fac = -1.0_8 / DX
  plane = (N1*N2)
  bcfac = Tau*fac
  bcterm = bcfac*u0

  DO K = 1, N3
    DO J = 1, N2
        LI = (K-1)*plane + (J-1)*N1 + 1
        KN(LI) = ( U(LI+1) - U(LI) ) * FAC + bcfac*U(LI) - bcterm
    END DO
  END DO

END SUBROUTINE RHSLEFTBOUNDARY


! Use non-zero tau to specify boundary data
SUBROUTINE RHSRIGHTBOUNDARY(N1, N2, N3, DX, U, Tau, u0, KN)

  INTEGER(KIND=8), INTENT(IN) :: N1, N2, N3
  REAL(KIND=8), INTENT(IN)    :: U(N1*N2*N3), DX, Tau, u0
  REAL(KIND=8), INTENT(OUT)   :: KN(N1*N2*N3)

  INTEGER(KIND=8) :: J, K, LI, plane
  REAL(KIND=8)    :: fac,bcfac,bcterm

  fac = -1.0_8 / DX
  plane = (N1*N2)
  bcfac = Tau*fac
  bcterm = bcfac*u0

  DO K = 1, N3
    DO J = 1, N2
        LI = (K-1)*plane + (J-1)*N1 + N1
        KN(LI) = ( U(LI) - U(LI-1) ) * FAC + bcfac*U(LI) - bcterm
    END DO
  END DO

END SUBROUTINE RHSRIGHTBOUNDARY

SUBROUTINE RHSHALOLEFT(N1, N2, N3, DX, U, KN, HL)

  INTEGER(KIND=8), INTENT(IN) :: N1, N2, N3
  REAL(KIND=8), INTENT(IN)    :: U(N1*N2*N3), HL(N2*N3), DX
  REAL(KIND=8), INTENT(OUT)   :: KN(N1*N2*N3)

  INTEGER(KIND=8) :: J, K, LI, plane, LII
  REAL(KIND=8)    :: fac

  fac = -0.5_8/DX
  plane = (N1*N2)

  DO K = 1, N3
     DO J = 1, N2
        LII = (K-1)*N2 + J
        LI = (K-1)*plane + (J-1)*N1 + 1
        KN(LI) = ( U(LI+1) - HL(LII) ) * FAC
     END DO
  END DO
  
END SUBROUTINE RHSHALOLEFT

SUBROUTINE RHSHALORIGHT(N1, N2, N3, DX, U, KN, HR)

  INTEGER(KIND=8), INTENT(IN) :: N1, N2, N3
  REAL(KIND=8), INTENT(IN) :: U(N1*N2*N3), HR(N2*N3), DX
  REAL(KIND=8), INTENT(OUT) :: KN(N1*N2*N3)

  INTEGER(KIND=8) :: J, K, LI, plane, LII
  REAL(KIND=8)    :: fac

  fac = -0.5_8/DX
  plane = (N1*N2)

  DO K = 1, N3
     DO J = 1, N2
        LII = (K-1)*N2 + J
        LI = (K-1)*plane + (J-1)*N1 + N1
        KN(LI) = ( HR(LII) - U(LI-1) ) * FAC
     END DO
  END DO
  
END SUBROUTINE RHSHALORIGHT
