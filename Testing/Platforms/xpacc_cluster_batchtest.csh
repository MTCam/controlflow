#!/bin/bash -fe

set app=${1}
set nodes=${2}
set config=${3}
set addarg=${4}
#rm -f tmpresults_2.txt
nproc = $(expr ${nodes} \* 24)
cat <<EOF > ./codelet4_quicktest.csh
#!/bin/bash
#$ -S /bin/bash
#$ -j y
#$ -o codelet4_quicktest.out
#$ -pe orte ${nproc}
#$ -l h_rt=00:30:00
##PBS -l nodes=${nodes}:ppn=24
##PBS -l walltime=00:30:00 

printf "Hostfile: \${PE_HOSTFILE}\n"
printf "-------------------------\n"
cat \${PE_HOSTFILE}
#env
printf "-------------------------\n"
printf "mpiexec -n ${nproc} -hostfile \${PE_HOSTFILE} ${app} -v 10 -p -c ${config} ${addarg}\n"
mpiexec -n ${nproc} -hostfile \${PE_HOSTFILE} ${app} -v 10 -p -c ${config} ${addarg}
EOF
set echo
set jobid=`qsub -cwd codelet4_quicktest.csh | cut -d " " -f 3`
unset echo
set joboutfile="codelet4_quicktest.csh.o${jobid}"
@ i = 1
while($i <= 720)
    @ i += 1
    if( -e ${joboutfile} ) then
        @ i += 721;
    else
        sleep 5;
    endif
end
printf "Codelet4 quicktest complete.\n"
#rm -f ./codelet4_quicktest.csh
