#!/bin/tcsh

set outFile=${1}
set binDir=${3}
set joboutfile="codelet4_parallel_test_batch_output"
#rm -f tmpresults_2.txt
rm -f ./codelet4_parallel_test_batch.csh
rm -f ${joboutfile}
cat <<EOF > ./codelet4_parallel_test_batch.csh
#!/bin/tcsh
#
#PBS -l nodes=1:ppn=12
#PBS -l walltime=00:30:00 
#PBS -j oe
#PBS -o ${joboutfile}
#PBS -q cse
#PBS -W group_list=csar

# cd \${PBS_O_WORKDIR}
mpiexec ${binDir}/codelet4x -v 10 -p
EOF

set echo
set jobid=`qsub codelet4_parallel_test_batch.csh`
unset echo
@ i = 1
while($i <= 720)
    @ i += 1
    if( -e ${joboutfile} ) then
        @ i += 721;
    else
        sleep 5;
    endif
end
printf "Codelet4:RunsInParallel=" >> ${outFile}
@ err = 0
set count=`grep Statistics ${joboutfile} | wc -l`
if ( ${count} == "1") then
   printf "1\n" >> ${outFile}
else
   printf "0\n" >> ${outFile}
   @ err += 1
endif
#set RESULTS=`cat tmpresults_2.txt | grep 3.141592653589`
#printf "PEPI:Works=" >> $1
#if ( "$RESULTS" == "") then
#   printf "0\n" >> ${outFile}
#   @ err += 1
#else
#   printf "1\n" >> ${outFile}
#endif  
#rm -f tmpresults_2.txt
exit ${err}
