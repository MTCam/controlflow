#!/bin/tcsh

set OutFile=$1
set TmpOut=${OutFile}_tmp.txt
set BINDIR=${3}

set HOSTNAME=`hostname -s`
set RUNCOMMAND=""
if( "${HOSTNAME}" =~ "vulcan*") then
  set RUNCOMMAND="srun -n 1 -ppdebug -t 2 "
endif
if( "${HOSTNAME}" =~ "cab*") then
  set RUNCOMMAND="srun -n 1 -ppdebug -t 2 "
endif
if( "${HOSTNAME}" =~ "syrah*") then
  set RUNCOMMAND="srun -n 1 -ppdebug -t 2 "
endif
# Test the serial example program output
set echo 
rm -rf codelet4x_serial.out
${RUNCOMMAND} ${BINDIR}/codelet4x -v 3 -p >& codelet4x_serial.out
printf "Codelet4:RunsInSerial=" > ${TmpOut}
if (! -e "Codelet4Timing_000001.txt") then
   echo "Couldn't find codelet4x profiling result."
   printf "0\n" >> ${TmpOut}
else
  printf "1\n" >> ${TmpOut}
  printf "Codelet4:Runs=1\n" >> ${TmpOut}
endif
set STEST=`cat codelet4x_serial.out | grep Hello`
printf "Codelet4:CanCallFortran=" >> ${TmpOut}
if("${STEST}" == "") then
  printf "0\n" >> ${TmpOut}
else
  printf "1\n" >> ${TmpOut}
endif

rm -rf Codelet4Timing_000001.txt
if( ! -e ${OutFile} ) then
    cat ${TmpOut} > ${OutFile}
else
    cat ${TmpOut} >> ${OutFile}
endif
rm -f ${TmpOut}
exit 0
