#!/bin/tcsh

set outFile=${1}
set binDir=${3}

srun -n 16 -ppdebug -t 5 ${binDir}/codelet4x -v 10 -p 
if(! -e ${outFile}) then
    printf "Codelet4:RunsInParallel=" > ${outFile}
else
    printf "Codelet4:RunsInParallel=" >> ${outFile}
endif
@ err = 0
set count=`grep Statistics Codelet4Timing*txt | wc -l`
if ( ${count} == "1") then
   printf "1\n" >> ${outFile}
else
   printf "0\n" >> ${outFile}
   @ err += 1
endif
exit ${err}
