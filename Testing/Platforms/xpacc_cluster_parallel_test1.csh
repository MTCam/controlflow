#!/bin/tcsh

set outFile=${1}
set binDir=${3}


rm -f tmpresults_1.txt
cat <<EOF > ./codelet4_parallel_test_batch.csh
#!/bin/tcsh
#
#PBS -l nodes=2:ppn=8
#PBS -l walltime=00:30:00 
#PBS -j oe
#PBS -o codelet4_parallel_test_batch_output
##PBS -A XPACC_TEST

# cd \${PBS_O_WORKDIR}
mpiexec -n 16 ${binDir}/codelet4_parallel_test -o tmpresults_1.txt
EOF
qsub -cwd codelet4_parallel_test_batch.csh
@ i = 1
while($i <= 720)
    @ i += 1
    if( -e tmpresults_1.txt ) then
        @ i += 721;
    else
        sleep 5;
    endif
end
cat tmpresults_1.txt >> ${outFile}
#rm -f tmpresults_1.txt
#rm -f ./codelet4_parallel_test_batch.csh
#rm -f codelet4_parallel_test_batch_output
exit 0
